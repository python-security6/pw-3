import argparse
import json
import os
parser = argparse.ArgumentParser()
parser.add_argument("--key") 
parser.add_argument("--val")
args = parser.parse_args()

storage_path = os.path.join(os.getcwd(), 'storage.data')
print(storage_path)
if os.path.exists(storage_path):
    with open(storage_path, 'r') as f: 
        storage = json.load(f)
else:
    storage = {}

if args.key and args.val:
    if args.key in storage:
        if args.val not in storage[args.key]:
            storage[args.key].append(args.val) 
    else:
        storage[args.key] = [args.val] 
    with open(storage_path, 'w') as f:
        json.dump(storage, f)
elif args.key:
    print(', '.join(storage.get(args.key, [])))