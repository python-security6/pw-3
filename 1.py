import argparse
import csv
parser = argparse.ArgumentParser(description='Read and filter a CSV file')
parser.add_argument('-fieldnames', '--fieldnames', nargs='+', help='fieldnames have to be displayed')
parser.add_argument('-numrows', '--numrows', type=int, help='number of rows have to be displayed')
parser.add_argument('-f', '--fname', help='fname')
args = parser.parse_args()
with open(args.fname, 'r') as f: # 'r' for reading
    reader = csv.DictReader(f, delimiter=';')
    fieldnames = args.fieldnames
    numrows = args.numrows
    print(','.join(fieldnames))
    for row in reader:
        if numrows == 0:
            break 
        else:
            print(','.join(row[fieldname] for fieldname in fieldnames))
            numrows -= 1
